// MegaEnigma.ino - Timer.ino
// @arduinoenigma 2019

unsigned long exectimer[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};      /*xls*/
unsigned long maxtimes[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};      /*xls*/

// the timing functions add 10.6uS of overhead to caller
void entrytimer(byte task)     /*xls*/
{
  exectimer[task] = micros();
}


//
void exittimer(byte task)     /*xls*/
{
  unsigned long now = micros();
  unsigned long entry = exectimer[task];

  unsigned long t = now - entry;

  if (t > maxtimes[task])
  {
    maxtimes[task] = t;
  }
}


//
void printtimer()     /*xls*/
{
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(i);
    Serial.print(F(":"));
    Serial.println(maxtimes[i]);
  }
}


//
void endurancetimer(byte action)
{
  // 1st: customize data below, set the starting and ending memory addresses and the frequency to save data
  // then call endurancetimer(1); in the loop() function to clear the memory, compile and run
  // then call endurancetimer(2); in the loop() function to test the battery endurance, compile and run till the batteries give out (unplug usb)
  // then call endurancetimer(3); in the loop() function to print how long the device ran on batteries
  // do not change the variables below after calling endurancetimer(1);

  // customizable data:

  unsigned int firstbyte = 700;        // first memory location to use (use lastbyte=714) for short testing
  unsigned int lastbyte  = 4000;       // approximate last memory location, could be up to +3 bytes if not aligned to 4 byte boundary
  unsigned int intervalminutes = 1;    // save progress every x minutes

  //end of customizable data

  unsigned long now = millis();
  unsigned long actualinterval = intervalminutes * 60 * 1000;

  static byte state = 0;
  static unsigned long nextevent = 0; //interval * 10 * 1000;
  static unsigned int currentpoint = 0;

  switch (action)
  {
    case 1:
      {
        if (EEPROM.read(firstbyte) != 0)
        {
          Serial.print(F("clearing endurance timer"));

          unsigned long v = 0;
          unsigned int i = firstbyte;

          EEPROM.write(i, 0);
          i++;

          do
          {
            //Serial.println(i);
            eeprom_write_block((const void*)&v, (void*)i, 4);
            i += 4;
          } while (i < lastbyte);

          Serial.print(F(" done,"));
        }
        else
        {
          Serial.print(F("endurance timer already cleared,"));
        }

        Serial.print(F(" comment or change call to endurancetimer(2); and upload again\x0d\x0a"));

        do {} while (1); // hang here
        break;
      }

    case 3:
      {
        unsigned long t;
        unsigned long max1 = 0;
        unsigned long max2 = 0;

        for (unsigned int i = firstbyte + 1; i < (lastbyte); i += 4)
        {
          eeprom_read_block((void*)&t, (void*)(i), 4);

          Serial.print(i);
          Serial.print(':');
          Serial.println(t);

          if (t > max1)
          {
            max2 = max1;
            max1 = t;
          }
          else
          {
            if (t > max2)
            {
              max2 = t;
            }
          }
        }

        Serial.print(F("endurance run time (ms):"));

        if ((max1 - max2) != actualinterval)
        {
          Serial.println(max2);
        }
        else
        {
          Serial.println(max1);
        }

        Serial.print(F("to do another test, comment or change call to endurancetimer(1); and upload again\x0d\x0a"));

        do {} while (1); // hang here
        break;
      }

    default:
      {
        switch (state)
        {
          case 0:
            {
              byte v = EEPROM.read(firstbyte);

              if (v != 0)
              {
                Serial.print(F("endurance test completed, comment or change call to endurancetimer(3); and upload again\x0d\x0a"));
                state = 1;
              }
              else
              {
                Serial.print(F("running endurance test\x0d\x0a"));
                EEPROM.write(firstbyte, 42);
                currentpoint = firstbyte + 1;
                state = 2;
              }

              break;
            }

          case 1:
            {
              // nothing to do but exit back to caller quickly
              break;
            }

          case 2:
            {
              if (now > nextevent)
              {
                eeprom_write_block((const void*)&now, (void*)currentpoint, 4);
                nextevent += actualinterval;

                currentpoint += 4;

                if (currentpoint >= lastbyte)
                {
                  currentpoint = firstbyte + 1;
                }

                //Serial.println(currentpoint);
              }

              break;
            }
        }

        break;
      }
  }
}

