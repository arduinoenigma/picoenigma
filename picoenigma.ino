// PicoEnigma.ino
// @arduinoenigma 2020

//BUGS (may have been fixed, notes left in place for regression testing):
// type some letters, change the UHR, exit to main screen, rewind letters to AAAA, type letters amd they are wrong.
// selecting commonCathode/commonAnode wrong lights up some lampfield led, change display routine to input when display is blank so this does not happen
// check H key (dimly lit on white enigma) (in readKeys, set Segment.low() before Segment.output())
// sending !aaa after using menu encodes it instead of changing start position
// modify UpdateWheelData() so it only erases ukwd if changing the machine, but not the rotors or the ring
// changing MACH resets UKWD, ROTR, RINGS
// changing ROTR resets RING, not to overwrite UKWD
// changing RING not to overwrite UKWD
// changing brightness inside V07 not to overwrite UKWD
// needs a function to verify that the edited UKWD is valid, stay in menu if not
// cannot edit UKWD in UD mode
// if K->D and D->K, pressing them both needs to turn off both (N/C contacts are open on both keys/path to lamps)

// uses this fast pin library:
// https://github.com/mikaelpatel/Arduino-GPIO
// download the zip file and click Sketch->Include Library->Add .ZIP Library

// click File->Preferences->Show verbose output during: and enable "compilation" for warnings and "upload" for avrdude command line parameters

// todo:
//
// done:
//

#include "GPIO.h"
#include <EEPROM.h>

// see UniversalEnigmaEngine->DefaultEnigma()
#define DEFAULTMACHINEID 9

// enable line below to be able to move rotors while a key is pressed
//#define UnlockWheels 1

// disables printing routine in UniversalEnigma.ino -> printstep()
#define MegaEnigma 1

#define SCROLLSPEED 60
#define ZEROISETIMER 2600

//#define CommonAnode
#define CommonCathode

#define TASKWHOLEPLUGBOARD 0
#define TASKSINGLEPLUG 1
#define TASKTIMETASK 8
#define TASKDUMMYTASK 9

// functions that use optional parameters and are located in other tabs must be declared extern
extern void SetDisplayValues(byte index, byte Value, bool Scroll = false);
extern void doAction(byte key, bool reset = false);
extern void Display4(const __FlashStringHelper *pWord, bool Scroll = false);


void setup()     /*xls*/
{
  // put your setup code here, to run once:

  Serial.begin(9600);

  allKeysInput();
  allPlugsInput();

  //selectDigit() will select the appropriate line to output
  //call with 99 to turn off all common pins and make them inputs
  selectDigit(99);

  allSegmentOff();
  allSegmentOutput();

  //enable();

  Serial.print(F("PicoEnigma 20210502\x0d\x0a"));

  //testUHR(); //BUG: disable

  //ClearEEPROM(); //BUG: disable

  LoadEnigmaFromEEPROM(); //BUG: enable
  //dbgPrintEnigma(); //BUG: remove

  InitEnigma();
  CheckPlugPairs();

  SetInitialMachineID(GetMachineID());

  ShowRotorPositions();
}


void loop()     /*xls*/
{
  // put your main code here, to run repeatedly:

  // to test battery endurance
  //ClearEEPROM();
  //endurancetimer(1);       // 1st: uncomment and run once to enable endurance test, then comment again
  //endurancetimer(2);       // 2nd: uncomment to run the endurance test (plug batteries, open serial monitor, upload, unplug usb), then comment again
  //endurancetimer(3);       // 3rd: uncomment to print the results of the endurance test, then comment again
  //testSimulatePressingA(); // enable to simulate pressing and releasing the A key,

  if (GetAttractMode())
  {
    AttractMode(0);
  }

  readKeys();
  debounceKeys();
  processKeys();
  CheckZeroise();
  DecMenuTimer();

  DoEnigma();
  ProcessEnigma();

  ShowPlugs(255); // special item in menu, needs to be here to continuously scan the plugboard if enabled

  updateDisplay();
}

