// PicoEnigma.ino - DetectShorts.ino
// @arduinoenigma 2020

// a tool to detect if any pin is shorted to another
// used to check that the soldering job is correct
// to use, separate the carrier board from the display and keyboard pcb
// open the serial terminal, set to 9600 baud and send **1234
// this code will only execute if it receives the activation command as the first characters.

//
byte detectShort(byte a, byte b)      /*xls*/
{

  digitalWrite(a, LOW);
  pinMode(a, OUTPUT);
  digitalWrite(b, HIGH);
  pinMode(b, INPUT);

  delay(1);

  byte v = digitalRead(b);

  if ((v == 0) && (b != 13))
  {
    Serial.print(a);
    Serial.print('=');
    Serial.print(b);
    Serial.print(F("\x0d\x0a"));
  }
  pinMode(a, INPUT);
  digitalWrite(b, LOW);
  pinMode(b, INPUT);
}


//
void detectShorts()     /*xls*/
{
  for (byte i = 2; i < 53; i++)
  {
    for (byte j = 2; j < 53; j++)
    {
      detectShort(i, j);
    }
  }
  Serial.print(F("-----\x0d\x0a"));
}


//
void runDetectShorts(char k)     /*xls*/
{
  static byte state = 0;
  static byte good = 1;

  if (k != -1)
  {
    switch (state)
    {
      case 0:
        {
          if (k != '*')
          {
            good = 0;
          }
          break;
        }
      case 1:
        {
          if (k != '*')
          {
            good = 0;
          }
          break;
        }
      case 2:
        {
          if (k != '1')
          {
            good = 0;
          }
          break;
        }
      case 3:
        {
          if (k != '2')
          {
            good = 0;
          }
          break;
        }
      case 4:
        {
          if (k != '3')
          {
            good = 0;
          }
          break;
        }
      case 5:
        {
          if (k != '4')
          {
            good = 0;
          }
          break;
        }
    }

    if (state < 5)
    {
      state++;
    }
    else
    {
      state = 99;
      if (good)
      {
        good = 0;
        detectShorts();

        allKeysInput();
        allPlugsInput();
        selectDigit(99);
        allSegmentOff();
        allSegmentOutput();
      }
    }
  }
}

