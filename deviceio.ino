// PicoEnigma.ino - DeviceIO.ino
// @arduinoenigma 2020

#if defined(CommonCathode)
#define SegmentOn high
#define SegmentOff low
#define DigitOn low
#define DigitOff high
#define LampsOff high
#define LampsOn low
#endif

#if defined(CommonAnode)
#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low
#define LampsOff high
#define LampsOn low
#endif


GPIO<BOARD::D2>  CCDS2;
GPIO<BOARD::D3>  SegmentB;
GPIO<BOARD::D4>  CCDS1;
GPIO<BOARD::D5>  SegmentG1;
GPIO<BOARD::D6>  SegmentA1;
GPIO<BOARD::D7>  SegmentJ;
GPIO<BOARD::D8>  SegmentK;
GPIO<BOARD::D9>  SegmentF;
GPIO<BOARD::D10> SegmentH;
GPIO<BOARD::D11> CCDS4;
GPIO<BOARD::D12> CCDS3;
GPIO<BOARD::D13> D13NC;
GPIO<BOARD::D14> SegmentC;
GPIO<BOARD::D15> SegmentDP;
GPIO<BOARD::D16> SegmentD1;
GPIO<BOARD::D17> SegmentG2;
GPIO<BOARD::D18> SegmentL;
GPIO<BOARD::D19> SegmentM;
GPIO<BOARD::D20> SegmentN;
GPIO<BOARD::D21> SegmentE;
GPIO<BOARD::D22> PlugA;
GPIO<BOARD::D23> PlugB;
GPIO<BOARD::D24> PlugC;
GPIO<BOARD::D25> PlugD;
GPIO<BOARD::D26> PlugE;
GPIO<BOARD::D27> PlugF;
GPIO<BOARD::D28> PlugG;
GPIO<BOARD::D29> PlugH;
GPIO<BOARD::D30> PlugI;
GPIO<BOARD::D31> PlugJ;
GPIO<BOARD::D32> PlugK;
GPIO<BOARD::D33> PlugL;
GPIO<BOARD::D34> PlugM;
GPIO<BOARD::D35> PlugN;
GPIO<BOARD::D36> PlugO;
GPIO<BOARD::D37> PlugP;
GPIO<BOARD::D38> PlugQ;
GPIO<BOARD::D39> PlugR;
GPIO<BOARD::D40> UPDNL;
GPIO<BOARD::D41> PlugS;
GPIO<BOARD::D42> LED2L;
GPIO<BOARD::D43> LED1L;
GPIO<BOARD::D44> KEY2L;
GPIO<BOARD::D45> LED3L;
GPIO<BOARD::D46> KEY3L;
GPIO<BOARD::D47> KEY1L;
GPIO<BOARD::D48> PlugT;
GPIO<BOARD::D49> PlugU;
GPIO<BOARD::D50> PlugV;
GPIO<BOARD::D51> PlugW;
GPIO<BOARD::D52> PlugX;
GPIO<BOARD::D53> PlugY;


//A0..A15
GPIO<BOARD::D54> PlugZ;
//GPIO<BOARD::D55> ;
//GPIO<BOARD::D56> ;
//GPIO<BOARD::D57> ;
//GPIO<BOARD::D58> ;
//GPIO<BOARD::D59> ;
//GPIO<BOARD::D60> ;
//GPIO<BOARD::D61> ;
//GPIO<BOARD::D62> ;
//GPIO<BOARD::D63> ;
//GPIO<BOARD::D64> ;
//GPIO<BOARD::D65> ;
//GPIO<BOARD::D66> ;
//GPIO<BOARD::D67> ;
//GPIO<BOARD::D68> ;
//GPIO<BOARD::D69> ;


void allSegmentOutput()     /*xls*/
{
  SegmentA1.output();
  //SegmentA2.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD1.output();
  //SegmentD2.output();
  SegmentDP.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG1.output();
  SegmentG2.output();
  SegmentH.output();
  SegmentJ.output();
  SegmentK.output();
  SegmentL.output();
  SegmentM.output();
  SegmentN.output();
}


void allSegmentInput()     /*xls*/
{
  SegmentA1.input();
  //SegmentA2.input();
  SegmentB.input();
  SegmentC.input();
  SegmentD1.input();
  //SegmentD2.input();
  SegmentDP.input();
  SegmentE.input();
  SegmentF.input();
  SegmentG1.input();
  SegmentG2.input();
  SegmentH.input();
  SegmentJ.input();
  SegmentK.input();
  SegmentL.input();
  SegmentM.input();
  SegmentN.input();
}


// used in display context to turn off all segments
void allSegmentOff()     /*xls*/
{
  SegmentA1.SegmentOff();
  //SegmentA2.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD1.SegmentOff();
  //SegmentD2.SegmentOff();
  SegmentDP.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG1.SegmentOff();
  SegmentG2.SegmentOff();
  SegmentH.SegmentOff();
  SegmentJ.SegmentOff();
  SegmentK.SegmentOff();
  SegmentL.SegmentOff();
  SegmentM.SegmentOff();
  SegmentN.SegmentOff();
}


// used in key reading context to set all the segments to high
void allSegmentHigh()     /*xls*/
{
  SegmentA1.high();
  //SegmentA2.high();
  SegmentB.high();
  SegmentC.high();
  SegmentD1.high();
  //SegmentD2.high();
  SegmentDP.high();
  SegmentE.high();
  SegmentF.high();
  SegmentG1.high();
  SegmentG2.high();
  SegmentH.high();
  SegmentJ.high();
  SegmentK.high();
  SegmentL.high();
  SegmentM.high();
  SegmentN.high();
}


// used in lampfield displaying context to set all the segments to low
void allSegmentLow()     /*xls*/
{
  SegmentA1.low();
  //SegmentA2.low();
  SegmentB.low();
  SegmentC.low();
  SegmentD1.low();
  //SegmentD2.low();
  SegmentDP.low();
  SegmentE.low();
  SegmentF.low();
  SegmentG1.low();
  SegmentG2.low();
  SegmentH.low();
  SegmentJ.low();
  SegmentK.low();
  SegmentL.low();
  SegmentM.low();
  SegmentN.low();
}


void allDigitsOutput()     /*xls*/
{
  CCDS1.output();
  CCDS2.output();
  CCDS3.output();
  CCDS4.output();
}


void allDigitsInput()     /*xls*/
{
  CCDS1.input();
  CCDS2.input();
  CCDS3.input();
  CCDS4.input();
}


void allDigitsOff()     /*xls*/
{
  CCDS1.DigitOff();
  CCDS2.DigitOff();
  CCDS3.DigitOff();
  CCDS4.DigitOff();
}


void allLampsOutput()     /*xls*/
{
  LED1L.output();
  LED2L.output();
  LED3L.output();
}


void allLampsInput()     /*xls*/
{
  LED1L.input();
  LED2L.input();
  LED3L.input();
}


void allLampsOff()     /*xls*/
{
  LED1L.LampsOff();
  LED2L.LampsOff();
  LED3L.LampsOff();
}


void allKeysInput()     /*xls*/
{
  KEY1L.input();
  KEY1L.high();

  KEY2L.input();
  KEY2L.high();

  KEY3L.input();
  KEY3L.high();

  UPDNL.input();
  UPDNL.high();
}


void allPlugsInput()     /*xls*/
{
  PlugA.input();
  PlugB.input();
  PlugC.input();
  PlugD.input();
  PlugE.input();
  PlugF.input();
  PlugG.input();
  PlugH.input();
  PlugI.input();
  PlugJ.input();
  PlugK.input();
  PlugL.input();
  PlugM.input();
  PlugN.input();
  PlugO.input();
  PlugP.input();
  PlugQ.input();
  PlugR.input();
  PlugS.input();
  PlugT.input();
  PlugU.input();
  PlugV.input();
  PlugW.input();
  PlugX.input();
  PlugY.input();
  PlugZ.input();

  PlugA.high();
  PlugB.high();
  PlugC.high();
  PlugD.high();
  PlugE.high();
  PlugF.high();
  PlugG.high();
  PlugH.high();
  PlugI.high();
  PlugJ.high();
  PlugK.high();
  PlugL.high();
  PlugM.high();
  PlugN.high();
  PlugO.high();
  PlugP.high();
  PlugQ.high();
  PlugR.high();
  PlugS.high();
  PlugT.high();
  PlugU.high();
  PlugV.high();
  PlugW.high();
  PlugX.high();
  PlugY.high();
  PlugZ.high();
}

