// MegaEnigma.ino - Scroller.ino
// @arduinoenigma 2019

//
uint16_t scrollup(byte d, byte times)     /*xls*/
{
  uint16_t raw = getRaw(d);

  for (byte i = 0; i < times; i++)
  {
    raw = raw << 1;
    raw = raw & 0b1011010101101010;
  }

  return raw;
}


//
uint16_t scrolldn(byte d, byte times)     /*xls*/
{
  uint16_t raw = getRaw(d);

  for (byte i = 0; i < times; i++)
  {
    raw = raw >> 1;
    raw = raw & 0b0101101010110101;
  }

  return raw;
}


// scrollstep goes 0..4
uint16_t stepUp(byte src, byte dest, byte scrollstep)     /*xls*/
{
  uint16_t raw;

  raw = scrolldn(src, scrollstep) | scrollup (dest, 4 - scrollstep);

  return raw;
}


//
uint16_t stepDown(byte src, byte dest, byte scrollstep)     /*xls*/
{
  uint16_t raw;

  raw = scrolldn(dest, 4 - scrollstep) | scrollup (src, scrollstep);

  return raw;
}

