// MegaEnigma.ino - Plugboard.ino
// @arduinoenigma 2019

// a state machine that scans a plug and returns -1 (scan in progress), or value of other end of plug or itself if no plug installed
// runtime: 20us
signed char TestPlug(byte letter)     /*xls*/
{
  static byte state = 0;
  static byte sourceplug = 0;
  static byte destinationplug = 0;
  static signed char plugfound = -1;
  static signed char  retvalue = -1;
  static unsigned long entryt;

  //Serial.print(F("s:"));
  //Serial.print(state);
  //Serial.print(F(" src:"));
  //Serial.print(sourceplug);
  //Serial.print(F(" dst:"));
  //Serial.print(destinationplug);
  //Serial.print(F(" fnd:"));
  //Serial.println(plugfound);

  entrytimer(TASKSINGLEPLUG);

  if (letter == 255)
  {
    state = 0;
    return 0;
  }

  switch (state)
  {
    case 0:
      if (letter != 0)
      {
        state = 1;
        sourceplug = letter;
        destinationplug = 0;
        plugfound = -1;
        retvalue = -1;
        allPlugsInput();
      }
      break;
    case 1:
      {
        state = 2;
        entryt = micros();
        switch (sourceplug)
        {
          case 1:
            PlugA.low();
            PlugA.output();
            break;
          case 2:
            PlugB.low();
            PlugB.output();
            break;
          case 3:
            PlugC.low();
            PlugC.output();
            break;
          case 4:
            PlugD.low();
            PlugD.output();
            break;
          case 5:
            PlugE.low();
            PlugE.output();
            break;
          case 6:
            PlugF.low();
            PlugF.output();
            break;
          case 7:
            PlugG.low();
            PlugG.output();
            break;
          case 8:
            PlugH.low();
            PlugH.output();
            break;
          case 9:
            PlugI.low();
            PlugI.output();
            break;
          case 10:
            PlugJ.low();
            PlugJ.output();
            break;
          case 11:
            PlugK.low();
            PlugK.output();
            break;
          case 12:
            PlugL.low();
            PlugL.output();
            break;
          case 13:
            PlugM.low();
            PlugM.output();
            break;
          case 14:
            PlugN.low();
            PlugN.output();
            break;
          case 15:
            PlugO.low();
            PlugO.output();
            break;
          case 16:
            PlugP.low();
            PlugP.output();
            break;
          case 17:
            PlugQ.low();
            PlugQ.output();
            break;
          case 18:
            PlugR.low();
            PlugR.output();
            break;
          case 19:
            PlugS.low();
            PlugS.output();
            break;
          case 20:
            PlugT.low();
            PlugT.output();
            break;
          case 21:
            PlugU.low();
            PlugU.output();
            break;
          case 22:
            PlugV.low();
            PlugV.output();
            break;
          case 23:
            PlugW.low();
            PlugW.output();
            break;
          case 24:
            PlugX.low();
            PlugX.output();
            break;
          case 25:
            PlugY.low();
            PlugY.output();
            break;
          case 26:
            PlugZ.low();
            PlugZ.output();
            break;
        }
        break;
      }
    case 2:
      {
        //Give UHR time to react to a plug going low
        unsigned long t = micros() - entryt;
        if (t > 1000)
        {
          state = 3;
        }
        break;
      }
    case 3:
      {
        destinationplug++;
        if (destinationplug == sourceplug)
        {
          destinationplug++;
        }

        if (destinationplug > 26) // two succesive increments can push us over 26
        {
          if (plugfound == -1)
          {
            plugfound = sourceplug;
          }
          state = 4;
        }

        byte val = 1;
        switch (destinationplug) {
          case 1:
            val = PlugA.read();
            break;
          case 2:
            val = PlugB.read();
            break;
          case 3:
            val = PlugC.read();
            break;
          case 4:
            val = PlugD.read();
            break;
          case 5:
            val = PlugE.read();
            break;
          case 6:
            val = PlugF.read();
            break;
          case 7:
            val = PlugG.read();
            break;
          case 8:
            val = PlugH.read();
            break;
          case 9:
            val = PlugI.read();
            break;
          case 10:
            val = PlugJ.read();
            break;
          case 11:
            val = PlugK.read();
            break;
          case 12:
            val = PlugL.read();
            break;
          case 13:
            val = PlugM.read();
            break;
          case 14:
            val = PlugN.read();
            break;
          case 15:
            val = PlugO.read();
            break;
          case 16:
            val = PlugP.read();
            break;
          case 17:
            val = PlugQ.read();
            break;
          case 18:
            val = PlugR.read();
            break;
          case 19:
            val = PlugS.read();
            break;
          case 20:
            val = PlugT.read();
            break;
          case 21:
            val = PlugU.read();
            break;
          case 22:
            val = PlugV.read();
            break;
          case 23:
            val = PlugW.read();
            break;
          case 24:
            val = PlugX.read();
            break;
          case 25:
            val = PlugY.read();
            break;
          case 26:
            val = PlugZ.read();
            break;
        }
        if (val == 0)
        {
          plugfound = destinationplug;
        }
        break;
      }

    case 4:
      {
        state = 0;
        retvalue = plugfound;
        break;
      }
  }

  exittimer(TASKSINGLEPLUG);

  return retvalue;
}

