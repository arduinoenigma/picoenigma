// MegaEnigma.ino - CharMap.ino
// @arduinoenigma 2019

/*
  implementing this would mean moving RawBits to 32 bits, maybe increase StepUp steps from 5 to 7

  a1 h g1 n d1

  a2 k g2 l d2

  f dummy e

  b dummy c

  a1 h g1 n d1 a2 k g2 l d2 f dummy e b dummy c
  0 1  1 1  1  0 1  1 1  1 0   1   1 0   1   1
  1 1  1 1  0  1 1  1 1  0 1   1   0 1   1   0
*/

/*
  current layout

  f e  a1 g1 d1  h n  j m  a2 g2 d2  k l b c

  1 0   1  1  0  1 0  1 0   1  1  0  1 0 1 0
  0 1   0  1  1  0 1  0 1   0  1  1  0 1 0 1
*/


//picoenigma
#define RawBitsBL 0b0000100110110001  /*  b  */
#define RawBitsPL 0b0010000111100010  /*  p  */
//#define RawBitsUL 0b0000100110010011  /*  tall u  */
#define RawBitsUL 0b0100100000010001  /*  short u  */
#define RawBitsDL 0b0000100010110011  /*  d  */


//megaenigma
//#define RawBitsBL 0b1101100010000000  /*  b  */
//#define RawBitsPL 0b1111000100000000  /*  p  */
//#define RawBitsUL 0b1100100110000000  /*  u  */
//#define RawBitsDL 0b0101100110000000  /*  d  */


#define RawBitsA 0b1111000001100011
#define RawBitsB 0b0010100111110011
#define RawBitsC 0b1110100001010000
#define RawBitsD 0b0010100111010011
#define RawBitsE 0b1111100001010000
#define RawBitsF 0b1111000001000000
#define RawBitsG 0b1110100001110001
#define RawBitsH 0b1101000000100011
#define RawBitsI 0b0010100111010000
#define RawBitsJ 0b0100100000010011
#define RawBitsK 0b1101000000001100
#define RawBitsL 0b1100100000010000
#define RawBitsM 0b1100010000001011
#define RawBitsN 0b1100010000000111
#define RawBitsO 0b1110100001010011
#define RawBitsP 0b1111000001100010
#define RawBitsQ 0b1110100001010111
#define RawBitsR 0b1111000001100110
#define RawBitsS 0b1011100001110001
#define RawBitsT 0b0010000111000000
#define RawBitsU 0b1100100000010011
#define RawBitsV 0b1100001000001000
#define RawBitsW 0b1100001000000111
#define RawBitsX 0b0000011000001100
#define RawBitsY 0b1001100000110011
#define RawBitsZ 0b0010101001011000
#define RawBits0 0b1110101001011011
#define RawBits1 0b0000000000001011
#define RawBits2 0b0111100001110010
#define RawBits3 0b0010100001110011
#define RawBits4 0b1001000000100011
#define RawBits5 0b1011100001010100
#define RawBits6 0b1111100001110001
#define RawBits7 0b0010000001000011
#define RawBits8 0b1111100001110011
#define RawBits9 0b1011100001110011

