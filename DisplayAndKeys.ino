// PicoEnigma.ino - DisplayAndKeys.ino
// @arduinoenigma 2020

#define displaymask1 0b00000000000000001111111111111111
#define displaymask2 0b11111111111111110000000000000000

// current value to display
byte DisplayValues[4] = {0, 0, 0, 0};     /*xls*/
// the next value to display
byte NewDisplayValues[4] = {0, 0, 0, 0};     /*xls*/

byte ScrollState[4] = {5, 5, 5, 5};     /*xls*/
byte ScrollDelay[4] = {0, 0, 0, 0};     /*xls*/
byte NeedsScroll[4] = {0, 0, 0, 0};     /*xls*/

uint32_t RawDisplayValues[7] = {0, 0, 0, 0, 0, 0, 0};    /*xls*/

byte KeysPressedCount = 0;     /*xls*/

byte InhibitLampfield = 2;     /*xls*/

//needs function that outputs 1 bit at a time and reads the keys for that bit, store in KeyPressed.
//every time a key is detected, store it here according to its position A..Z DS1u,DS2u,DS3u,DS4u,MENU,DS1d,DS2d,DS3d,DS4d
byte KeysPressed[35] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};     /*xls*/
// every time a key is stored in KeyPressed, also put a 255 in here. a Debounce() function will decrement and clear the KeysPressed entry when this geto to 0
byte KeysDebounce[35] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};     /*xls*/
// compare KeysPressed with KeysProcessed, if non-zero in KeysPressed and zero here, store it here and process it, if zero in KeysPressed, zero it here.
byte KeysProcessed[35] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};     /*xls*/

// store here what lamp each key lit up so we can turn it off later
byte LightUp[35] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};     /*xls*/


//
void selectDigit(byte digit)     /*xls*/
{
  allDigitsInput();
  allLampsInput();

  allDigitsOff();
  allLampsOff();

  switch (digit) {
    case 1:
      CCDS1.DigitOn();
      CCDS1.output();
      break;
    case 2:
      CCDS2.DigitOn();
      CCDS2.output();
      break;
    case 3:
      CCDS3.DigitOn();
      CCDS3.output();
      break;
    case 4:
      CCDS4.DigitOn();
      CCDS4.output();
      break;
    case 5:
      LED1L.LampsOn();
      LED1L.output();
      break;
    case 6:
      LED2L.LampsOn();
      LED2L.output();
      break;
    case 7:
      LED3L.LampsOn();
      LED3L.output();
      break;
  }
}


//
void displayRaw(uint32_t raw)     /*xls*/
{
  allSegmentOutput();

  if (raw & 0b10000000000000000) // ATTN: 17bit number (forced move to uint32_t)
  {
    SegmentDP.SegmentOn();
  }
  else
  {
    SegmentDP.SegmentOff();
  }

  if (raw & 0b1000000000000000)
  {
    SegmentF.SegmentOn();
  }
  else
  {
    SegmentF.SegmentOff();
  }

  if (raw & 0b0100000000000000)
  {
    SegmentE.SegmentOn();
  }
  else
  {
    SegmentE.SegmentOff();
  }

  if (raw & 0b0010000000000000)
  {
    SegmentA1.SegmentOn();
  }
  else
  {
    SegmentA1.SegmentOff();
  }

  if (raw & 0b0001000000000000)
  {
    SegmentG1.SegmentOn();
  }
  else
  {
    SegmentG1.SegmentOff();
  }

  if (raw & 0b0000100000000000)
  {
    SegmentD1.SegmentOn();
  }
  else
  {
    SegmentD1.SegmentOff();
  }

  if (raw & 0b0000010000000000)
  {
    SegmentH.SegmentOn();
  }
  else
  {
    SegmentH.SegmentOff();
  }

  if (raw & 0b0000001000000000)
  {
    SegmentN.SegmentOn();
  }
  else
  {
    SegmentN.SegmentOff();
  }

  if (raw & 0b0000000100000000)
  {
    SegmentJ.SegmentOn();
  }
  else
  {
    SegmentJ.SegmentOff();
  }

  if (raw & 0b0000000010000000)
  {
    SegmentM.SegmentOn();
  }
  else
  {
    SegmentM.SegmentOff();
  }

  if (raw & 0b0000000001000000)
  {
    //SegmentA2.SegmentOn();
  }
  else
  {
    //SegmentA2.SegmentOff();
  }

  if (raw & 0b0000000000100000)
  {
    SegmentG2.SegmentOn();
  }
  else
  {
    SegmentG2.SegmentOff();
  }

  if (raw & 0b0000000000010000)
  {
    //SegmentD2.SegmentOn();
  }
  else
  {
    //SegmentD2.SegmentOff();
  }

  if (raw & 0b0000000000001000)
  {
    SegmentK.SegmentOn();
  }
  else
  {
    SegmentK.SegmentOff();
  }

  if (raw & 0b0000000000000100)
  {
    SegmentL.SegmentOn();
  }
  else
  {
    SegmentL.SegmentOff();
  }

  if (raw & 0b0000000000000010)
  {
    SegmentB.SegmentOn();
  }
  else
  {
    SegmentB.SegmentOff();
  }

  if (raw & 0b0000000000000001)
  {
    SegmentC.SegmentOn();
  }
  else
  {
    SegmentC.SegmentOff();
  }
}


// give it a letter (A..Z 0..9) and it returns a 16 bit value for the display
uint16_t getRaw(byte letter)     /*xls*/
{
  switch (letter)
  {
    case 1:
      return RawBitsA;
    case 2:
      return RawBitsB;
    case 3:
      return RawBitsC;
    case 4:
      return RawBitsD;
    case 5:
      return RawBitsE;
    case 6:
      return RawBitsF;
    case 7:
      return RawBitsG;
    case 8:
      return RawBitsH;
    case 9:
      return RawBitsI;
    case 10:
      return RawBitsJ;
    case 11:
      return RawBitsK;
    case 12:
      return RawBitsL;
    case 13:
      return RawBitsM;
    case 14:
      return RawBitsN;
    case 15:
      return RawBitsO;
    case 16:
      return RawBitsP;
    case 17:
      return RawBitsQ;
    case 18:
      return RawBitsR;
    case 19:
      return RawBitsS;
    case 20:
      return RawBitsT;
    case 21:
      return RawBitsU;
    case 22:
      return RawBitsV;
    case 23:
      return RawBitsW;
    case 24:
      return RawBitsX;
    case 25:
      return RawBitsY;
    case 26:
      return RawBitsZ;

    case 27:
      return RawBits0;
    case 28:
      return RawBits1;
    case 29:
      return RawBits2;
    case 30:
      return RawBits3;
    case 31:
      return RawBits4;
    case 32:
      return RawBits5;
    case 33:
      return RawBits6;
    case 34:
      return RawBits7;
    case 35:
      return RawBits8;
    case 36:
      return RawBits9;

    case 38:
      return RawBitsBL;
    case 40:
      return RawBitsDL;
    case 52:
      return RawBitsPL;
    case 57:
      return RawBitsUL;

    default:
      return 0;
  }
}


//LED1L is in RawDisplayValues[4]
//LED2L is in RawDisplayValues[5]
//LED3L is in RawDisplayValues[6]
//picoenigma changed
void lightOn(byte digit)     /*xls*/
{
  switch (digit)
  {
    case 1:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00100000000000000; //A-E
      break;
    case 2:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000100000000000; //B-D1
      break;
    case 3:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000000010000000; //C-M
      break;
    case 4:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00000000000100000; //D-G2
      break;
    case 5:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000000000100000; //E-G2
      break;
    case 6:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00000000010000000; //F-M
      break;
    case 7:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00000000000000100; //G-L
      break;
    case 8:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00000100000000000; //H-D1
      break;
    case 9:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000000000000001; //I-C
      break;
    case 10:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b10000000000000000; //J-DP
      break;
    case 11:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00000000000000001; //K-C
      break;
    case 12:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000000000000010; //L-B
      break;
    case 13:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000000000000001; //M-C
      break;
    case 14:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b10000000000000000; //N-DP
      break;
    case 15:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000000000000010; //O-B
      break;
    case 16:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00100000000000000; //P-E
      break;
    case 17:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00100000000000000; //Q-E
      break;
    case 18:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000000010000000; //R-M
      break;
    case 19:
      RawDisplayValues[5] = RawDisplayValues[5] | 0b00000001000000000; //S-N
      break;
    case 20:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000000000000100; //T-L
      break;
    case 21:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b10000000000000000; //U-DP
      break;
    case 22:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000000000000100; //V-L
      break;
    case 23:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000001000000000; //W-N
      break;
    case 24:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000000000100000; //X-G2
      break;
    case 25:
      RawDisplayValues[6] = RawDisplayValues[6] | 0b00000001000000000; //Y-N
      break;
    case 26:
      RawDisplayValues[4] = RawDisplayValues[4] | 0b00000100000000000; //Z-D1
      break;
  }
}


//LED1L is in RawDisplayValues[4]
//LED2L is in RawDisplayValues[5]
//LED3L is in RawDisplayValues[6]
//picoenigma change
void lightOff(byte digit)     /*xls*/
{
  switch (digit)
  {
    case 1:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11011111111111111; //A-E
      break;
    case 2:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111011111111111; //B-D1
      break;
    case 3:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111111101111111; //C-M
      break;
    case 4:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111111111011111; //D-G2
      break;
    case 5:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111111111011111; //E-G2
      break;
    case 6:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111111101111111; //F-M
      break;
    case 7:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111111111111011; //G-L
      break;
    case 8:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111011111111111; //H-D1
      break;
    case 9:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111111111111110; //I-C
      break;
    case 10:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b01111111111111111; //J-DP
      break;
    case 11:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111111111111110; //K-C
      break;
    case 12:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111111111111101; //L-B
      break;
    case 13:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111111111111110; //M-C
      break;
    case 14:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b01111111111111111; //N-DP
      break;
    case 15:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111111111111101; //O-B
      break;
    case 16:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11011111111111111; //P-E
      break;
    case 17:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11011111111111111; //Q-E
      break;
    case 18:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111111101111111; //R-M
      break;
    case 19:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111110111111111; //S-N
      break;
    case 20:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111111111111011; //T-L
      break;
    case 21:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b01111111111111111; //U-DP
      break;
    case 22:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111111111111011; //V-L
      break;
    case 23:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111110111111111; //W-N
      break;
    case 24:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111111111011111; //X-G2
      break;
    case 25:
      RawDisplayValues[6] = RawDisplayValues[6] & 0b11111110111111111; //Y-N
      break;
    case 26:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111011111111111; //Z-D1
      break;
  }
}


//
void allLightsOff()     /*xls*/
{
  RawDisplayValues[4] = 0;
  RawDisplayValues[5] = 0;
  RawDisplayValues[6] = 0;
}


//
void allLightsOn()     /*xls*/
{
  RawDisplayValues[4] = 0b11111111111111111;
  RawDisplayValues[5] = 0b11111111111111111;
  RawDisplayValues[6] = 0b11111111111111111;
}


//
void LampFieldMessage(const __FlashStringHelper *string)     /*xls*/
{
  //const char PROGMEM *p = (const char PROGMEM *)string;
  const char  *p = (const char PROGMEM *)string;

  char k = 0;

  do
  {
    k = pgm_read_byte(p);

    if (k == 0)
    {
      break;
    }

    p++;

    lightOn(k - 'A' + 1);

  } while (1);
}


//A..Z 12345 (up keys and menu) abcd (down keys)
byte getKeyIndex(byte key)     /*xls*/
{
  byte index = 0;
  if ((key > 'A' - 1) && (key < 'Z' + 1))
  {
    index = key - 'A';
  }
  else if ((key > '1' - 1) && (key < '5' + 1))
  {
    index = key - '1' + 26;
  }
  else if ((key > 'a' - 1) && (key < 'd' + 1))
  {
    index = key - 'a' + 26 + 5;
  }
  return index;
}


//BUG: Test all of this
// used by readKeys() below, if the bus is active, the key is pressed
void processKeyBus(bool BusValue, byte key)     /*xls*/
{
  if (BusValue == 0)
  {
    byte index = getKeyIndex(key);
    KeysPressed[index] = key;
    KeysDebounce[index] = 3;
  }
}


//BUG: Test all of this
//
void readKeys()     /*xls*/
{
  static byte state = 0;

  //turn off all displays
  allDigitsInput();
  allLampsInput();

  allDigitsOff();
  allLampsOff();

  // to detect keys we will make one segment 0 at a time to see if it comes in on KEY1L...
  // we cannot depend on DisplayRaw since the polarity can change with display type...
  allSegmentInput(); // instead of putting 1 on the unselected pins, make them float so two keys can be pushed
  allSegmentHigh(); // in addition to unused pins being in HighZ, they also need to be pullup

  state++;
  if (state == 14)
  {
    state = 1;
  }

  //A..Z  65..90
  //abcd  up keys
  //1234  down keys
  //5     menu

  switch (state)
  {
    case 1:
      SegmentB.low();
      SegmentB.output();
      delayMicroseconds(2); // don't like it, but we must have a settling time to allow the segment to go low
      processKeyBus(UPDNL.read(), '5');
      processKeyBus(KEY1L.read(), 'O');
      processKeyBus(KEY3L.read(), 'L');
      break;
    case 2:
      SegmentG1.low();
      SegmentG1.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), '4');
      break;
    case 3:
      SegmentJ.low();
      SegmentJ.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), '3');
      break;
    case 4:
      SegmentC.low();
      SegmentC.output();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), 'I');
      processKeyBus(KEY2L.read(), 'K');
      processKeyBus(KEY3L.read(), 'M');
      break;
    case 5:
      SegmentD1.low();
      SegmentD1.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), 'd');
      processKeyBus(KEY1L.read(), 'Z');
      processKeyBus(KEY2L.read(), 'H');
      processKeyBus(KEY3L.read(), 'B');
      break;
    case 6:
      SegmentL.low();
      SegmentL.output();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), 'T');
      processKeyBus(KEY2L.read(), 'G');
      processKeyBus(KEY3L.read(), 'V');
      break;
    case 7:
      SegmentN.low();
      SegmentN.output();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), 'W');
      processKeyBus(KEY2L.read(), 'S');
      processKeyBus(KEY3L.read(), 'Y');
      break;
    case 8:
      SegmentE.low();
      SegmentE.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), 'a');
      processKeyBus(KEY1L.read(), 'Q');
      processKeyBus(KEY2L.read(), 'A');
      processKeyBus(KEY3L.read(), 'P');
      break;
    case 9:
      SegmentM.low();
      SegmentM.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), 'c');
      processKeyBus(KEY1L.read(), 'R');
      processKeyBus(KEY2L.read(), 'F');
      processKeyBus(KEY3L.read(), 'C');
      break;
    case 10:
      SegmentG2.low();
      SegmentG2.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), 'b');
      processKeyBus(KEY1L.read(), 'E');
      processKeyBus(KEY2L.read(), 'D');
      processKeyBus(KEY3L.read(), 'X');
      break;
    case 11:
      SegmentDP.low();
      SegmentDP.output();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), 'U');
      processKeyBus(KEY2L.read(), 'J');
      processKeyBus(KEY3L.read(), 'N');
      break;
    case 12:
      SegmentK.low();
      SegmentK.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), '1');
      break;
    case 13:
      SegmentA1.low();
      SegmentA1.output();
      delayMicroseconds(2);
      processKeyBus(UPDNL.read(), '2');
      break;
  }

  allSegmentOff();
  allSegmentOutput();
}


//BUG: Test all of this
//
void debounceKeys()     /*xls*/
{
  static byte state = 0;

  state++;
  if (state == 35)
  {
    state = 0;
  }

  if (KeysDebounce[state])
  {
    --KeysDebounce[state];
    if (KeysDebounce[state] == 0)
    {
      byte light = LightUp[state];
      if (light != 0)
      {
        lightOff(light - '0');
      }

      KeysPressed[state] = 0;
      KeysProcessed[state] = 0;
      LightUp[state] = 0;
      KeysPressedCount--;
    }
  }
}


//BUG: Test all of this
//
void processKeys()     /*xls*/
{
  static byte state = 0;

  state++;
  if (state == 35)
  {
    state = 0;
  }

  if ((KeysPressed[state]) && (KeysProcessed[state] == 0))
  {
    KeysProcessed[state] = KeysPressed[state];
    doAction(KeysPressed[state]);
    KeysPressedCount++;
  }
}


//
byte isPressed(byte keyIndex)     /*xls*/
{
  return KeysPressed[keyIndex];
}


//
void LampBrightnessDelay(byte level)     /*xls*/
{
  switch (level)
  {
    case 0:
      delayMicroseconds(25);
      break;
    case 1:
      delayMicroseconds(50);
      break;
    case 2:
      delayMicroseconds(100);
      break;
    case 3:
      delayMicroseconds(200);
      break;
    default:
      delayMicroseconds(350);
      break;
  }
}


// call this to get DisplayValues[] to show up on the display
void updateRawValues()     /*xls*/
{
  RawDisplayValues[0] = getRaw(DisplayValues[0]);
  RawDisplayValues[1] = getRaw(DisplayValues[1]);
  RawDisplayValues[2] = getRaw(DisplayValues[2]);
  RawDisplayValues[3] = getRaw(DisplayValues[3]);
}


//
bool LampfieldEnabled()     /*xls*/
{
  return (InhibitLampfield == 2);
}


//
void stopScroll()     /*xls*/
{
  for (byte i = 0; i < 4; i++)
  {
    ScrollState[i] = 5;
    NeedsScroll[i] = 0;
    ScrollDelay[i] = 0;
    DisplayValues[i] = NewDisplayValues[i];
  }

  InhibitLampfield = 2;
  updateRawValues();
}


//
void updateDisplay()     /*xls*/
{
  static byte state = 0;
  static byte statescroll = 0;

  byte src = DisplayValues[statescroll];
  byte dst = NewDisplayValues[statescroll];

  if ((src != dst) && (NeedsScroll[statescroll] == 0))
  {
    NeedsScroll[statescroll] = 1;
    ScrollState[statescroll] = 0;
    ScrollDelay[statescroll] = 0;
  }

  if (ScrollState[statescroll] != 5)
  {
    InhibitLampfield = 0;

    ++ScrollDelay[statescroll];
    if (ScrollDelay[statescroll] > SCROLLSPEED) // 80
    {
      if (((dst > src) || ((src == 26) && (dst == 1))) && !((src == 1) && (dst == 26)))
      {
        RawDisplayValues[statescroll] = stepUp(src, dst, ScrollState[statescroll]);
      }
      else if ((dst < src) || ((src == 1) && (dst == 26)))
      {
        RawDisplayValues[statescroll] = stepDown(src, dst, ScrollState[statescroll]);
      }

      ScrollDelay[statescroll] = 0;
      ++ScrollState[statescroll];

      if (ScrollState[statescroll] == 5)
      {
        NeedsScroll[statescroll] = 0;
        DisplayValues[statescroll] = NewDisplayValues[statescroll];
        RawDisplayValues[statescroll] = getRaw(DisplayValues[statescroll]);
      }
    }
  }

  //put this after the scroll logic so the code sees it from 0..3
  statescroll++;
  if (statescroll == 4)
  {
    statescroll = 0;
    //logic to detect when scrolling is finished
    if (InhibitLampfield < 2)
    {
      InhibitLampfield++;
    }
  }

  //put this one here so the display logic sees it from 1..11
  state++;
  if (state == 12)
  {
    state = 1;
  }

  switch (state)
  {
    case 1:
      selectDigit(1);
      displayRaw(RawDisplayValues[0] & displaymask1);  // splitting each digit in two equalizes the brightness between 1/7 and 8/9
      delayMicroseconds(200);
      break;
    case 2:
      selectDigit(2);
      displayRaw(RawDisplayValues[1] & displaymask1);
      delayMicroseconds(200);
      break;
    case 3:
      selectDigit(3);
      displayRaw(RawDisplayValues[2] & displaymask1);
      delayMicroseconds(200);
      break;
    case 4:
      selectDigit(4);
      displayRaw(RawDisplayValues[3] & displaymask1);
      delayMicroseconds(200);
      break;

    case 5:
      selectDigit(1);
      displayRaw(RawDisplayValues[0] & displaymask2); // second half
      delayMicroseconds(200);
      break;
    case 6:
      selectDigit(2);
      displayRaw(RawDisplayValues[1] & displaymask2);
      delayMicroseconds(200);
      break;
    case 7:
      selectDigit(3);
      displayRaw(RawDisplayValues[2] & displaymask2);
      delayMicroseconds(200);
      break;
    case 8:
      selectDigit(4);
      displayRaw(RawDisplayValues[3] & displaymask2);
      delayMicroseconds(200);
      break;

    case 9:
      selectDigit(5);                       // lampfield
      if (InhibitLampfield == 2)
      {
        displayRaw(RawDisplayValues[4]);
      }
      LampBrightnessDelay(GetBrightness());               // BUG: this gets changed to a variable, configurable via menu  "bL 1..5"
      break;
    case 10:
      selectDigit(6);
      if (InhibitLampfield == 2)
      {
        displayRaw(RawDisplayValues[5]);
      }
      LampBrightnessDelay(GetBrightness());
      break;
    case 11:
      selectDigit(7);
      if (InhibitLampfield == 2)
      {
        displayRaw(RawDisplayValues[6]);
      }
      LampBrightnessDelay(GetBrightness());
      break;
  }

  selectDigit(99); // turn display off
}


//
void SetDisplayValues(byte index, byte Value, bool Scroll = false)     /*xls*/
{
  NewDisplayValues[index] = Value;

  if (Scroll == false)
  {
    DisplayValues[index] = Value;
  }

  updateRawValues();
}


//
byte TextToIndex(byte key)     /*xls*/
{
  byte index = 0;
  if ((key > 'A' - 1) && (key < 'Z' + 1))
  {
    index = key - 'A' + 1;
  }
  else if ((key > '0' - 1) && (key < '9' + 1))
  {
    index = key - '0' + 27;
  }
  else if ((key > 'a' - 1) && (key < 'z' + 1))
  {
    index = key - 'a' + 37;
  }
  return index;
}


//
void Display4(const __FlashStringHelper *pWord, bool Scroll = false)     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)pWord;
  const char *p = (const char *)pWord;

  for (byte i = 0; i < 4; i++)
  {
    SetDisplayValues(i, TextToIndex(pgm_read_byte(p + i)), Scroll);
  }
}


//
void SelfTestMenu()     /*xls*/
{
  static byte state = 26;

  //26 27 28 29
  //31 32 33 34
  //rotor up/down keys can be used to
  if ((KeysPressed[state]) && (KeysProcessed[state] == 0))
  {
    KeysProcessed[state] = KeysPressed[state];
    switch (state)
    {
      case 26:
        {
          //BUG: machine is undeterminate, but calling DefaultEnigma shows machine type on display
          //SetMachineID(DEFAULTMACHINEID);
          //SetInitialMachineID(DEFAULTMACHINEID);
          SetAttractMode(2); // set to 2 so attract mode is saved in eeprom once
          AttractMode(1); // reset the playback pointer to the begining of the sequence
          break;
        }
      case 29:
        {
          SetBrightness(GetBrightness() - 1);
          Serial.print(F("\x0d\x0a er={\x0d\x0a er>er>X"));
          break;
        }
      case 34:
        {
          SetBrightness(GetBrightness() + 1);
          Serial.print(F("\x0d\x0a er=}\x0d\x0a er>er>X"));
          break;
        }
    }
  }
  state++;
  if (state == 35)
  {
    state = 26;
  }
}


// this function takes over the updateDisplay duties, does not return until menu is pressed and released
// lights up the whole lampfield sequentally, then lets user press a key to turn that lampfield lamp off
// exits when all the lights have been turned off or menu is pressed. Press menu at any time to cancel and exit
void LampsAndKeysSelfTest()     /*xls*/
{
  byte KeyStatus[26];

  //const __FlashStringHelper *pLampOrder = F("QWERTZUIOASDFGHJKPYXCVBNML");   // left to right one row at a time
  const __FlashStringHelper *pLampOrder = F("QWERTZUIOKJHGFDSAPYXCVBNML");   // snake, top row left to right, middle row right to left

  //const PROGMEM char *p = (const char PROGMEM *)pLampOrder;
  const char *p = (const char *)pLampOrder;

  allLightsOff();

  Display4(F("8888"));

  for (byte i = 0; i < 26; i++)
  {
    byte l;

    //l = i + 1 //regular A-Z order
    l = pgm_read_byte(p + i) - 'A' + 1; //order set by pLampOrder above

    lightOn(l); //order set by pLampOrder above

    Serial.print(F("\x0d\x0a"));
    Serial.print(F("er>er>"));
    Serial.print((char)(l + 'A' - 1));

    KeyStatus[i] = 1;

    /*
      for (byte j = 0; j < 4; j++)
      {
      RawDisplayValues[j] = 0b11111111111111111; // lights up the whole 16 segment display
      }
    */

    for (int j = 0; j < 2000; j++)
    {
      readKeys();

      SelfTestMenu();

      debounceKeys();
      CheckZeroise();
      updateDisplay();
    }

    if (KeysPressed[30])
    {
      break; // exit this loop, continue executing rest of function
    }
  }

  Serial.print(F("\x0d\x0a"));

  byte k = 0;
  byte cnt = 26;
  do
  {
    readKeys();

    SelfTestMenu();

    if ((KeysPressed[k] != 0) && (KeyStatus[k] != 0))
    {
      Serial.print(F("er>er>"));
      Serial.print((char)(k + 'A'));
      Serial.print(F("\x0d\x0a"));

      lightOff(k + 1);
      KeyStatus[k] = 0;
      cnt--;
    }
    k++;
    if (k > 25)
    {
      k = 0;
    }

    debounceKeys();
    CheckZeroise();
    updateDisplay();
  } while ((KeysPressed[30] == 0) && (cnt != 0)); //exit when menu is pressed

  do
  {
    cnt = 0;
    for (byte i = 0; i < 26; i++)
    {
      readKeys();
      debounceKeys();
      CheckZeroise();
      updateDisplay();
      if (KeysPressed[i]) {
        cnt++;
      }
    }
  } while ((KeysPressed[30] != 0) || (cnt != 0)); //wait for menu and all other letter keys to be released then return

  allLightsOff();
  Serial.print(F("\x0d\x0a"));
}


//
void CheckZeroise()     /*xls*/
{
  static long cnt = 0;

  if (KeysPressed[30])
  {
    cnt++;
  }
  else
  {
    cnt = 0;
  }

  if (cnt == ZEROISETIMER)
  {
    allLightsOff();
    ShowPlugs(0); // turn off plug visualization in case we were in plug menu
    DefaultEnigma();
    InitSWPlugs();
    SetMenuTimer();

    SaveEnigmaToEEPROM();     //BUG: clear EEPROM

    doAction(0, true); // reset the status of the menu
    for (byte i = 0; i < 4; i++)
    {
      if (GetRotorPosition(i) != 0)
      {
        SetRotorPosition(i, 'X');
      }
    }
    ShowRotorPositions();
  }
}


//
// to enable attract mode:
// 1) hold down menu to clear parameters
// 2) push menu until v17 is shown
// 3) push the key above the leftmost display two times
// 4) push the menu button to save and exit to the main screen.
// the demo sequence starts playing right away
//
// when simulator is powered up, it goes through the demo sequence.
//
// to exit attract mode:
// hold down the menu button to clear all parameters.
//
// print these instructions and tape to the bottom of the simulator.
//
void AttractMode(byte restart)    /*xls*/
{
  //
  // this is the attract mode demo sequence.
  // the sequence starts in coding mode so, letters A..Z are encoded.
  // manu button: 5
  // down keys: 1 2 3 4
  //   up keys: a b c d
  // the initial machine model cannot be known, but can be forced by sending 5 followed by a letter for a unique machine like I, N, T
  //
  const __FlashStringHelper *AttractPGM = F("AAAAAA5ddddddddddddddddddddddd5dddddddddcbaa5dddcccbbbbaaaaa5dcddbcccdddd55\x00");
  static byte ndx = 0;
  static byte state = 0;
  static byte v = 0;
  static int sendcount = 0;
  static int pausecount = 0;

  if (restart)
  {
    ndx = 0;
  }

  switch (state)
  {
    case 0:
      {
        v = pgm_read_byte((const char *)AttractPGM + ndx);
        ndx++;

        if (v == 0)
        {
          ndx = 0;
        }
        else
        {
          if ((v > 'A' - 1) && (v < 'Z' + 1))
          {
            sendcount = 5000;
            pausecount = 2000;
          }
          else
          {
            sendcount = 30;

            if (v == '5')
            {
              pausecount = 6000;
            }
            else
            {
              pausecount = 4000;
            }
          }

          state = 1;
        }

        break;
      }

    case 1:
      {
        sendcount--;
        if (sendcount)
        {
          processKeyBus(0, v);
        }
        else
        {
          state = 2;
        }

        break;
      }

    case 2:
      {
        pausecount--;
        if (pausecount == 0)
        {
          state = 0;
        }

        break;
      }
  }
}


//
// 0 1 2 3 4 5 6 7 8 9 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2
//                     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
// A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
//
void enable()     /*xls*/
{
  byte match = 0;
  byte k = 0;
  byte cnt = 0;

  byte userinput[10];
  //byte userexit[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  byte userexit[10] = {0, 0, 12, 6, 24, 10, 22, 17, 5, 15}; // AAMGYKWRFP

  //EEPROM.write(130, 42); // uncomment and run once to enable then comment for normal use

  byte x = EEPROM.read(130);

  if (x != 42)
  {
    return;
  }

  allLightsOff();

  for (byte i = 0; i < 10; i++)
  {
    userinput[i] = 0;
  }

  do
  {
    readKeys();
    debounceKeys();
    updateDisplay();

    if ((KeysPressed[k]) && (KeysProcessed[k] == 0))
    {
      KeysProcessed[k] = KeysPressed[k];

      match = 1;
      for (byte i = 0; i < 9; i++)
      {
        userinput[i] = userinput[i + 1];
        if (userinput[i] != userexit[i])
        {
          match = 0;
        }
      }
      userinput[9] = k;
      if (userinput[9] != userexit[9])
      {
        match = 0;
      }
    }

    k++;

    if (k == 26)
    {
      k = 0;
    }

  } while (match == 0);

  EEPROM.write(130, 255);

  do
  {
    cnt = 0;
    for (byte i = 0; i < 26; i++)
    {
      readKeys();
      debounceKeys();
      updateDisplay();
      if (KeysPressed[i]) {
        cnt++;
      }
    }
  } while ((KeysPressed[30] != 0) || (cnt != 0)); //wait for menu and all other letter keys to be released then return
}

