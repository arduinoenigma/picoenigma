// MegaEnigma.ino - Tests.ino
// @arduinoenigma 2019

//
// moral of the story:
// to print a space, use Serial.print(F(" "));
// to print a newline, use Serial.print(F("\x0d\x0a"));
//
//  Only prints AB
//  Sketch uses 1,794 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 182 bytes (2%) of dynamic memory, leaving 8,010 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.print(F("HELLO WORLD"));
//  Sketch uses 1,812 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 182 bytes (2%) of dynamic memory, leaving 8,010 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.print("HELLO WORLD");
//  Sketch uses 1,820 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 194 bytes (2%) of dynamic memory, leaving 7,998 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.print(F(" "));
//  Sketch uses 1,802 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 182 bytes (2%) of dynamic memory, leaving 8,010 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.print(F("\x0d\x0a"));
//  Sketch uses 1,804 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 182 bytes (2%) of dynamic memory, leaving 8,010 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.print(" ");
//  Sketch uses 1,810 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 184 bytes (2%) of dynamic memory, leaving 8,008 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.print(' ');
//  Sketch uses 1,816 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 182 bytes (2%) of dynamic memory, leaving 8,010 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.println(F(""));
//  Sketch uses 1,820 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 186 bytes (2%) of dynamic memory, leaving 8,006 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.println("");
//  Sketch uses 1,826 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 186 bytes (2%) of dynamic memory, leaving 8,006 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.println((char)0);
//  Sketch uses 1,834 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 186 bytes (2%) of dynamic memory, leaving 8,006 bytes for local variables. Maximum is 8,192 bytes.
//
//  Serial.println(' ');
//  Sketch uses 1,834 bytes (0%) of program storage space. Maximum is 253,952 bytes.
//  Global variables use 186 bytes (2%) of dynamic memory, leaving 8,006 bytes for local variables. Maximum is 8,192 bytes.
//
void testSerialSpace()     /*xls*/
{
  Serial.begin(9600);
  Serial.print(F("A"));

  //Serial.print(F("HELLO WORLD")); // adds 18 bytes code,  0 bytes ram
  //Serial.print("HELLO WORLD");    // adds 26 bytes code, 12 bytes ram

  //Serial.print(F(" "));           // adds  8 bytes code,  0 bytes ram
  //Serial.print(F("\x0d\x0a"));    // adds 10 bytes code,  0 bytes ram
  //Serial.print(" ");              // adds 16 bytes code,  2 bytes ram (string gets copied to ram)
  //Serial.print(' ');              // adds 22 bytes code,  0 bytes ram

  //Serial.println(F(""));          // adds 26 bytes code,  4 bytes ram
  //Serial.println("");             // adds 32 bytes code,  4 bytes ram
  //Serial.println((char)0);        // adds 40 bytes code,  4 bytes ram
  //Serial.println(' ');            // adds 40 bytes code,  4 bytes ram

  Serial.print(F("B"));
}


//
void testSimulatePressingA()     /*xls*/
{
  static unsigned int kd = 0;

  kd++;

  if (kd > 1500)
  {
    processKeyBus(0, 'A');
  }

  if (kd == 7000)
  {
    kd = 0;
  }
}


// A and B are A..Z
void testAddPlug(byte pair, byte left, byte right)     /*xls*/
{
  Serial.println(pair);
  SetPlugPair(pair, 0, (left - 'A') + 1);
  SetPlugPair(pair, 1, (right - 'A') + 1);
}


//
void testUHR()     /*xls*/
{
  byte pair = 0;
  InitSWPlugs();

  Serial.print(F("pairs: "));
  Serial.println(CheckPlugPairs());

  testAddPlug(pair++, 'A', 'E');
  testAddPlug(pair++, 'B', 'F');
  testAddPlug(pair++, 'C', 'M');
  testAddPlug(pair++, 'D', 'Q');
  testAddPlug(pair++, 'H', 'U');
  testAddPlug(pair++, 'J', 'N');
  testAddPlug(pair++, 'L', 'X');
  testAddPlug(pair++, 'P', 'R');
  testAddPlug(pair++, 'S', 'Z');
  testAddPlug(pair++, 'V', 'W');

  Serial.print(F("pairs: "));
  Serial.println(CheckPlugPairs());

  SetUHR(39);

  dbgPrintEnigma();

  ActivateUHR();

  dbgPrintEnigma();
}


//call often to illuminate all the letters that have plugs installed
//this is a state machine that calls a state machine, it takes time
//in real life, the enigma encryption only goes through the plugboard twice (52 calls of TestPlug)
void testShowPlug()     /*xls*/
{
  static byte plug = 1;
  signed char destplug = TestPlug(plug);

  if (destplug != -1)
  {
    if (destplug != plug)
    {
      lightOn(plug);
      lightOn(destplug);
    }
    else
    {
      lightOff(plug);
    }

    plug++;
    if (plug == 27)
    {
      plug = 1;
    }
  }
}


//
void testIncDisplay()     /*xls*/
{
  NewDisplayValues[0] = DisplayValues[0] + 1;
  //NewDisplayValues[0] = 0;

  if (NewDisplayValues[0] > 36)
  {
    NewDisplayValues[0] = 1;
  }
}


//
void testBNumber()     /*xls*/
{
  Serial.println(B00000001);
  Serial.println(0b00000001);
  Serial.println(B00000010);
  Serial.println(0b00000010);
  Serial.println(0b10000000000000000);
  Serial.println(0b1111111111111111);
  Serial.println(0b1000000000000000);
}


// takes 2131uS to execute 200 entry/exit time pairs (adds 10.6us of overhead to function)
void TestTaskTimer()     /*xls*/
{
  entrytimer(TASKTIMETASK);
  for (byte i = 0; i < 200; i++)
  {
    entrytimer(TASKDUMMYTASK);
    exittimer(TASKDUMMYTASK);
  }
  exittimer(TASKTIMETASK);
  printtimer();
}


void TestGetRaw()     /*xls*/
{
  Serial.println(getRaw(3));
  Serial.println(getRaw(2));
  Serial.println(getRaw(1));
  Serial.println(getRaw(4));
}


//
void TimeGetRaw()     /*xls*/
{
  byte c;
  Serial.println(F("timing getRaw"));
  entrytimer(1);
  for (byte i = 0; i < 200; i++)
  {
    c |= getRaw(i);
  }
  exittimer(1);
  Serial.println(c);
  printtimer();
}


//
void testTestPlug()     /*xls*/
{
  TestPlug(0);
  TestPlug(1);

  if (TestPlug(0) != -1)
  {
    TestPlug(3);
  }
}


//
void TestGetKeyIndex()     /*xls*/
{
  Serial.println(F("AB..YZ 15 ad"));
  Serial.println(getKeyIndex('A'));
  Serial.println(getKeyIndex('B'));
  Serial.println(getKeyIndex('Y'));
  Serial.println(getKeyIndex('Z'));
  Serial.println(getKeyIndex('1'));
  Serial.println(getKeyIndex('5'));
  Serial.println(getKeyIndex('a'));
  Serial.println(getKeyIndex('d'));
}


//
void testDisplayKeysPressed()     /*xls*/
{
  for (byte i = 0; i < 35; i++)
  {
    Serial.print((char)KeysPressed[i]);
  }
  Serial.print(F(" "));
  for (byte i = 0; i < 35; i++)
  {
    Serial.print(KeysDebounce[i]);
  }
  Serial.print(F(" "));
  for (byte i = 0; i < 35; i++)
  {
    Serial.print((char)KeysProcessed[i]);
  }
  Serial.println(F(" "));
}


const __FlashStringHelper *testcurrentwheel;      /*xls*/
const __FlashStringHelper *testcurrentstepnotches;      /*xls*/


//
void PrintCurrentWheel()     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)testcurrentwheel;
  const char *p = (const char *)testcurrentwheel;
  byte i = 0;
  char k = 0;

  do
  {
    k = pgm_read_byte(p + i);
    i++;

    Serial.print((byte)k);
    Serial.print(F(" "));
    Serial.print(k);
    Serial.print(F(" "));

  } while (k != 0);

  Serial.println(F(" "));
}


//
void PrintCurrentStepNotches()     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)testcurrentstepnotches;
  const char *p = (const char *)testcurrentstepnotches;
  byte i = 0;
  char k = 0;

  do
  {
    k = pgm_read_byte(p + i);
    i++;

    Serial.print((byte)k);
    Serial.print(F(" "));
    Serial.print(k);
    Serial.print(F(" "));

  } while (k != 0);

  Serial.println(F(" "));
}


//
void TestGetCurrentWheel(byte machine, byte wheel)     /*xls*/
{
  entrytimer(0);
  switch (machine)
  {
    case 1:
      {
        switch (wheel)
        {
          case 1:
            {
              testcurrentwheel = F("abcdefg");
              testcurrentstepnotches = F("jk");
              break;
            }
          case 2:
            {
              testcurrentwheel = F("xyz");
              testcurrentstepnotches = F("mn");
              break;
            }
        }
        break;
      }

    case 2:
      {
        switch (wheel)
        {
          case 1:
            {
              break;
            }
          case 2:
            {
              break;
            }
        }
        break;
      }
  }
  exittimer(0);
}


//
void TestGetCurrentWheel1()     /*xls*/
{
  TestGetCurrentWheel(1, 1);
  PrintCurrentWheel();
  PrintCurrentStepNotches();
  Serial.println(F(" "));

  TestGetCurrentWheel(1, 2);
  PrintCurrentWheel();
  PrintCurrentStepNotches();
  Serial.println(F(" "));

  TestGetCurrentWheel(1, 1);
  PrintCurrentWheel();
  PrintCurrentStepNotches();
  Serial.println(F(" "));

  printtimer();
}

